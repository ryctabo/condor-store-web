import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { LayoutModule } from './layout/layout.module';

import { RootComponent } from './layout/root/root.component';
import { HomeModule } from './home/home.module';
import { ProductModule } from './product/product.module';
import { ShoppingCartModule } from './shopping-cart/shopping-cart.module';
import { SettingsModule } from './settings/settings.module';
import { CategoryModule } from './category/category.module';

@NgModule({
  declarations: [],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,

    LayoutModule,
    HomeModule,
    ProductModule,
    ShoppingCartModule,
    SettingsModule,
    CategoryModule
  ],
  providers: [],
  bootstrap: [RootComponent]
})
export class AppModule { }
