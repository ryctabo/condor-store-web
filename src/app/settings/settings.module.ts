import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { ProductModule } from '../product/product.module';

import { SettingsComponent } from './settings.component';

@NgModule({
  declarations: [SettingsComponent],
  imports: [
    CommonModule,

    /** Local Modules */
    SharedModule,
    ProductModule
  ]
})
export class SettingsModule { }
