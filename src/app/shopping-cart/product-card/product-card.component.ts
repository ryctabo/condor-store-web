import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ShoppingCartService } from '../shared/shopping-cart.service';
import { Product } from 'src/app/product/shared/product.model';

@Component({
  selector: 'cdr-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product: Product;

  @Output() updateList = new EventEmitter();

  constructor(private service: ShoppingCartService) { }

  ngOnInit() { }

  onClickRemoveItem(product: Product) {
    this.service.removeProduct(product);
    this.updateList.emit();
  }

}
