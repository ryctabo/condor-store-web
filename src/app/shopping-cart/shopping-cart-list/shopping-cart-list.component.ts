import { Component, OnInit } from '@angular/core';
import { ShoppingCartService } from '../shared/shopping-cart.service';
import { Product } from 'src/app/product/shared/product.model';

@Component({
  selector: 'cdr-shopping-cart-list',
  templateUrl: './shopping-cart-list.component.html',
  styleUrls: ['./shopping-cart-list.component.scss']
})
export class ShoppingCartListComponent implements OnInit {

  products: Product[];

  constructor(private service: ShoppingCartService) { }

  ngOnInit() {
    this.sycnDataSource();
  }

  sycnDataSource() {
    this.products = this.service.getProducts() || [];
  }

}
