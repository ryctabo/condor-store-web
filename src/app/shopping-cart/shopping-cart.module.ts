import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';

import { ShoppingCartListComponent } from './shopping-cart-list/shopping-cart-list.component';
import { ProductCardComponent } from './product-card/product-card.component';

@NgModule({
  declarations: [
    ShoppingCartListComponent,
    ProductCardComponent
  ],
  imports: [
    CommonModule,

    /** Material Design Modules */
    MatCardModule,

    /** Local Modules */
    SharedModule
  ]
})
export class ShoppingCartModule { }
