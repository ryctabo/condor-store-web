import { Injectable } from '@angular/core';
import { Product } from 'src/app/product/shared/product.model';
import { ProductService } from 'src/app/product/shared/product.service';

const SHOPPING_CART_STORAGE_ID = 'condorlabs.storage.shoppingCart';

@Injectable({
  providedIn: 'root'
})
export class ShoppingCartService {

  constructor() { }

  getProducts(): Product[] {
    const shoppingCart = localStorage.getItem(SHOPPING_CART_STORAGE_ID);
    return shoppingCart ? JSON.parse(shoppingCart) : undefined;
  }

  addProduct(product: Product) {
    let products = this.getProducts();
    if (!products) { products = []; }
    products.push(product);
    localStorage.setItem(SHOPPING_CART_STORAGE_ID, JSON.stringify(products));
  }

  removeProduct(product: Product) {
    const products = this.getProducts();
    if (products) {
      products.splice(products.indexOf(product), 1);
      localStorage.setItem(SHOPPING_CART_STORAGE_ID, JSON.stringify(products));
    }
  }

  removeProductById(productId: number) {
    const products = this.getProducts();
    if (products) {
      products.forEach((p, index) => {
        if (p.id === productId) {
          products.splice(index, 1);
        }
      });
      localStorage.setItem(SHOPPING_CART_STORAGE_ID, JSON.stringify(products));
    }
  }

}
