import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatBadgeModule } from '@angular/material/badge';
import { MatMenuModule, MatRippleModule } from '@angular/material';

import { SharedModule } from '../shared/shared.module';

import { RootComponent } from './root/root.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MainComponent } from './main/main.component';
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    RootComponent,
    NotFoundComponent,
    MainComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,

    /** Material Design Modules */
    MatToolbarModule,
    MatBadgeModule,
    MatMenuModule,
    MatRippleModule,

    /** Local Modules */
    SharedModule
  ]
})
export class LayoutModule { }
