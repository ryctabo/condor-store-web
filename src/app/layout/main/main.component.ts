import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { CategoryService } from './../../category/shared/category.service';

import { Category } from './../../category/shared/category.model';

@Component({
  selector: 'cdr-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  categories: Category[] = [];

  categoryNameSelected = 'All Products';

  search: string;

  constructor(private categoryService: CategoryService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.categoryService.get().subscribe(
      (data: Category[]) => this.categories = data,
      error => console.log(error)
    );

    setTimeout(() => {
      this.route.queryParams.subscribe(
        queryParams => {
          if (this.categories && this.categories.length && queryParams.category) {
            const category = this.categories.find(c => c.id === +queryParams.category);
            this.categoryNameSelected = category ? category.name : 'Error';
          } else {
            this.categoryNameSelected = 'All Products';
          }
          this.search = queryParams.q;
        }
      );
    }, 100);
  }

  onClickMenuItem(category?: Category) {
    if (category) {
      this.categoryNameSelected = category.name;
      const url = this.router.url.split('\?')[0];
      this.router.navigate(['/'], {
        queryParams: {
          category: category.id
        },
        queryParamsHandling: 'merge'
      });
    } else {
      this.categoryNameSelected = 'All Products';
      this.router.navigate(['/']);
    }
  }

  onSearch() {
    this.router.navigate([], {
      queryParams: {
        q: this.search
      },
      queryParamsHandling: 'merge'
    });
  }

}
