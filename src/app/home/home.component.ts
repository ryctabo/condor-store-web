import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent } from '@angular/material';

import { Product } from '../product/shared/product.model';

import { ProductService } from '../product/shared/product.service';
import { QueryParams, QueryResult } from '../shared/http.service';

@Component({
  selector: 'cdr-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  dataSource: QueryResult<Product> = {
    items: [],
    total: 0
  };

  queryParams: QueryParams;

  constructor(private productService: ProductService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.route.queryParams.subscribe(
      queryParams => {
        this.queryParams = Object.assign({}, queryParams);
        if (+(queryParams.category) === 0) {
          delete this.queryParams['category'];
        }
        this._loadDataSource(this.queryParams);
      }
    );
  }

  private _loadDataSource(params: QueryParams) {
    params.size = 18;
    this.productService.get(params).subscribe(
      (data: QueryResult<Product>) => this.dataSource = data,
      error => console.log(error)
    );
  }

  onPage(pageInfo: PageEvent) {
    this.router.navigate(['/'], {
      queryParams: {
        start: pageInfo.pageIndex * pageInfo.pageSize,
        size: pageInfo.pageSize
      },
      queryParamsHandling: 'merge'
    });
  }

}
