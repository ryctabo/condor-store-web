import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatPaginatorModule } from '@angular/material';

import { ProductModule } from '../product/product.module';
import { SharedModule } from '../shared/shared.module';

import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,

    /** Material Design Modules */
    MatPaginatorModule,

    /** Custom Modules */
    ProductModule,
    SharedModule
  ]
})
export class HomeModule { }
