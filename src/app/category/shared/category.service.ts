import { Injectable } from '@angular/core';
import { HttpService } from 'src/app/shared/http.service';
import { Category } from './category.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService extends HttpService<Category> {

  get endpoint(): string { return 'categories'; }

  constructor(http: HttpClient) { super(http); }

}
