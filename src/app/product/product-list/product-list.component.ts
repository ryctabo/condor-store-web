import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PageEvent, MatSelectChange } from '@angular/material';

import { ProductService } from '../shared/product.service';
import { CategoryService } from 'src/app/category/shared/category.service';

import { Category } from 'src/app/category/shared/category.model';
import { QueryResult, QueryParams } from 'src/app/shared/http.service';
import { Product } from '../shared/product.model';

@Component({
  selector: 'cdr-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit {

  displayedColumns = ['id', 'name', 'price', 'category', 'created', 'updated', 'actions'];

  categories: Category[] = [];

  dataSource: QueryResult<Product> = {
    items: [],
    total: 0
  };

  queryParams: QueryParams;

  constructor(private productService: ProductService,
              private categoryService: CategoryService,
              private route: ActivatedRoute,
              private router: Router) { }

  ngOnInit() {
    this.categoryService.get().subscribe((data: Category[]) => this.categories = data);
    this.route.queryParams.subscribe(
      queryParams => {
        this.queryParams = Object.assign({}, queryParams);
        if (+(queryParams.category) === 0) {
          delete this.queryParams['category'];
        }

        this._loadDataSource(this.queryParams);
        console.log(this.queryParams);
      }
    );
  }

  private _loadDataSource(params: QueryParams) {
    this.productService.get(params).subscribe(
      (data: QueryResult<Product>) => this.dataSource = data,
      errorResponse => console.log(errorResponse)
    );
  }

  onSearch(value: string) {
    const url = this.router.url.split('\?')[0];
    this.router.navigate([url], {
      queryParams: {
        q: value
      },
      queryParamsHandling: 'merge'
    });
  }

  onPage(pageInfo: PageEvent) {
    const url = this.router.url.split('\?')[0];
    this.router.navigate([url], {
      queryParams: {
        start: pageInfo.pageIndex * pageInfo.pageSize,
        size: pageInfo.pageSize
      },
      queryParamsHandling: 'merge'
    });
  }

  onClean(input: any) {
    input.value = '';
    this.onSearch('');
  }

  onSelectionChange(select: MatSelectChange) {
    const url = this.router.url.split('\?')[0];
    this.router.navigate([url], {
      queryParams: {
        category: select.value
      },
      queryParamsHandling: 'merge'
    });
  }
}
