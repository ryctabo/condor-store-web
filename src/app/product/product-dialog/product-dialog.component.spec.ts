import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import {
  MatDialogModule,
  MatSnackBarModule,
  MatDialogRef,
  MAT_DIALOG_DATA
} from '@angular/material';

import { ProductDialogComponent } from './product-dialog.component';

describe('ProductDialogComponent', () => {
  let component: ProductDialogComponent;
  let fixture: ComponentFixture<ProductDialogComponent>;

  const dummyProduct = {
    name: '',
    description: '',
    category: {
      name: ''
    },
    created: new Date(),
    price: 0
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ProductDialogComponent],
      imports: [
        MatDialogModule,
        MatSnackBarModule
      ],
      providers: [{
        provide: MatDialogRef,
        useValue: {}
      }, {
        provide: MAT_DIALOG_DATA,
        useValue: dummyProduct
      }]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
