import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';

import { Product } from '../shared/product.model';
import { ShoppingCartService } from 'src/app/shopping-cart/shared/shopping-cart.service';

@Component({
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {

  constructor(private dialogRef: MatDialogRef<ProductDialogComponent>,
              @Inject(MAT_DIALOG_DATA) public product: Product,
              private shoppingCartService: ShoppingCartService,
              private snackBar: MatSnackBar) { }

  ngOnInit() { }

  onClickAddToShoppingCart() {
    this.shoppingCartService.addProduct(this.product);
    this.snackBar.open(
      `${this.product.name} has been added in the shopping cart!`,
      null,
      { duration: 1500 }
    );
    this.dialogRef.close();
  }

}
