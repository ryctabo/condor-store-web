import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTableModule } from '@angular/material/table';
import { MatSelectModule } from '@angular/material/select';
import { MatPaginatorModule } from '@angular/material/paginator';

import { SharedModule } from '../shared/shared.module';

import { ProductCardComponent } from './product-card/product-card.component';
import { ProductDialogComponent } from './product-dialog/product-dialog.component';
import { ProductListComponent } from './product-list/product-list.component';

@NgModule({
  declarations: [
    ProductCardComponent,
    ProductDialogComponent,
    ProductListComponent
  ],
  imports: [
    CommonModule,

    /** Material Design Modules */
    MatCardModule,
    MatDialogModule,
    MatTableModule,
    MatSelectModule,
    MatPaginatorModule,

    /** Local Modules */
    SharedModule
  ],
  exports: [
    ProductCardComponent,
    ProductListComponent
  ],
  entryComponents: [ProductDialogComponent]
})
export class ProductModule { }
