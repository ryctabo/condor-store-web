import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material';

import { ProductDialogComponent } from '../product-dialog/product-dialog.component';

import { Product } from '../shared/product.model';

@Component({
  selector: 'cdr-product-card',
  templateUrl: './product-card.component.html',
  styleUrls: ['./product-card.component.scss']
})
export class ProductCardComponent implements OnInit {

  @Input() product: Product;

  constructor(private dialog: MatDialog) { }

  ngOnInit() {
  }

  onClick(productSelected: Product) {
    this.dialog.open(ProductDialogComponent, {
      width: '720px',
      data: productSelected
    });
  }

}
