export class AppSettings {

  public static get baseUrl(): string {
    return 'http://localhost:8080/api.condorstore/v1';
  }

  public static getApiEndpoint(endpoint: string): string {
    return `${AppSettings.baseUrl}/${endpoint}`;
  }

}
