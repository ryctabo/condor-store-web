import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getImageSrc() {
    return element(by.css('cdr-root cdr-main header img')).getAttribute('src') as Promise<string>;
  }
}
